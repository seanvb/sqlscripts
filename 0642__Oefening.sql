USE `aptunes`;
DROP PROCEDURE IF EXISTS `CleanupOldMemberships`;



DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(IN someDate DATE, OUT numberCleaned INT)
BEGIN
	declare totalMembership int;
    select count(*) into totalMembership from Lidmaatschappen;
    set sql_safe_updates = 0;
    delete from Lidmaatschappen
    where Einddatum < someDate;
    set sql_safe_updates = 1;
    set numberCleaned = totalMembership- (select count(*) from Lidmaatschappen);
END$$

DELIMITER ;