USE ModernWaysBL;
SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes
CROSS JOIN Artiesten
SET Liedjes.Artiesten_Id = Artiesten.Id
WHERE Liedjes.Artiest = Artiesten.Naam;
SET SQL_SAFE_UPDATES = 1;