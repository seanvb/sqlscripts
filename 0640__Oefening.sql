USE `aptunes`;
DROP PROCEDURE IF EXISTS `GetLiedjes`;



DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN nameLiedjes VARCHAR(50))
BEGIN
    select *
    from Liedjes
    where Titel like CONCAT('%',nameLiedjes,'%');
END$$

DELIMITER ;