-- MAAKT TABEL PERSONEN AAN INDIEN NOG NIET BESTAAT
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- VOEGT DATA IN TABEL PERSONEN EN GEBRUIKEN DISTINCT OM EEN RIJ VOOR ELKE AUTEUR OVER TE HOUDEN.
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;

-- RANGSCHIKKEN OP VOORNAAM EN ACHTERNAAM VAN DE TABEL PERSONEN
use ModernWays;
select Voornaam, Familienaam from Personen
    order by Voornaam, Familienaam;

-- TABEL PERSONEN AANPASSEN
use ModernWays;
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
-- ALLES SELECTEREN UIT TABEL PERSONEN EN RANGSCHIKKEN OP FAMILIENAAM EN VOORNAAM
select * from Personen order by Familienaam, Voornaam;

-- TABEL PERSONEN AANPASSEN EN PERSONEN_ID NIET VERPLICHT MAKEN
use ModernWays;
alter table Boeken add Personen_Id int null;

-- WAARDE PERSONEN_ID KOPIËREN
select Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;

-- UPDATE VAN TABEL MET INSTRUCTIES DOOR DE SET CLAUSULE    
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
 
-- TABEL AANPASSEN EN WAARDE VERPLICHT MAKEN
alter table Boeken change Personen_Id Personen_Id int not null;

select Voornaam, Familienaam, Personen_Id from Boeken;

-- KOLOMMEN VERWIJDEREN UIT TABEL BOEKEN
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
-- GEBRUIK VAN CONSTRAINT OM TYPE VAN DATA TE LIMITEREN
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
   
