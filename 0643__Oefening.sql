USE `aptunes`;
DROP PROCEDURE IF EXISTS `CreateAndReleaseAlbum`;



DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateAndReleaseAlbum`(IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
	start transaction;
	insert into Albums(Titel) values(titel);
    insert into AlbumReleases(Bands_Id,Albums_Id)
    values(bands_Id,last_insert_id());
    commit;
END$$

DELIMITER ;