USE ModernWaysBL;
SELECT Artiest, SUM(Aantalbeluisteringen) AS 'Aantal beluisteringen'
FROM Liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) >= 100;