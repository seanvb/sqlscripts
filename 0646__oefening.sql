USE `aptunes`;
DROP procedure IF EXISTS `aptunes`.`MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleases`(in extraReleases int)
BEGIN
declare counter int default 0;
repeat 
call MockAlbumReleaseSucces(@succes);
if @succes=1 then set counter=counter + 1;
end if;
until counter>=extraReleases
end repeat;

END$$

DELIMITER ;
;
