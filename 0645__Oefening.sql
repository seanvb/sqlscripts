USE `aptunes`;
DROP procedure IF EXISTS `aptunes`.`MockAlbumReleaseWithSucces`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseWithSucces`(out succes bool)
BEGIN
declare numberOfAlbums INT DEFAULT 0; 
declare numberOfBands INT DEFAULT 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
select COUNT(*)
into numberOfAlbums from albums;
select COUNT(*)
into numberOfBands from bands;
set randomAlbumId =FLOOR(RAND() * numberOfAlbums)+1;
set randomBandId=FLOOR(RAND()*numberOfBands)+1;

if ((select COUNT(*) from Albumreleases where Bands_Id=randomBandId)<1 and (select COUNT(*) from Albumreleases where Albums_Id=randomAlbumId)<1 ) then set succes =1;
insert into albumreleases (Bands_Id,Albums_Id) values (randomBandId,randomAlbumId);
else set succes=0;
end if;

END$$

DELIMITER ;
;