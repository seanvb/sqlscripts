USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleasesLoop`(in extraReleases int)
BEGIN
declare counter int default 0;
justALoop: Loop

if counter >= extraReleases then
leave justALoop;
else 
call MockAlbumReleaseSucces(@succes);
if @succes=1 then select counter;
set counter=counter + 1;
end if;
end if;
end loop;

END$$

DELIMITER ;