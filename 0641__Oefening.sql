USE `aptunes`;
DROP PROCEDURE IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT totalGenres TINYINT)
BEGIN
	select count(*)
    into totalGenres
    from Genres;
END$$

DELIMITER ;