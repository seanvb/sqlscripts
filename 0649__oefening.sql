USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
declare randomGen int;
declare exit handler for SQLSTATE '45002' select 'State 45002 ontvangen, er bevindt zich geen probleem.';
declare exit handler for sqlexception select 'ERROR: State niet ontvangen!';
set randomGen=rand()*3+1;

if (randomGen=2) 
then signal sqlstate '45002';
elseif (randomGen=3) then
signal sqlstate '45003';
else signal sqlstate '45001';
end if;

END$$

DELIMITER ;