CREATE DATABASE IF NOT EXISTS databasetest;
USE databasetest;

CREATE TABLE Student(
    Studentennummer int PRIMARY KEY,
    Familienaam VARCHAR(255),
    Voornaam VARCHAR(255)
    );

CREATE TABLE Opleiding(
    Naam VARCHAR(255)
    );
    
CREATE TABLE Vak(
    Opleidingsonderdeel VARCHAR(255),
    Naam VARCHAR(255)
    );
    
CREATE TABLE Lector(
    Personeelsnummer int PRIMARY KEY,
    Familienaam VARCHAR(255),
    Voornaam VARCHAR(255)
    );
