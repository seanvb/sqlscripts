USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetAlbumDuration`(IN album int,out totalDuration int)
BEGIN

declare songDuration TINYINT unsigned;
declare albumDuration smallint unsigned default 0;
declare ok int default 0;
declare songLength 
cursor for select Lengte FROM liedjes

where Albums_Id= album;

declare continue handler for NOT FOUND set ok=1;

OPEN songLength;
getLength: LOOP
fetch songLength into songDuration;

if ok=1 then leave getLength;
end if;

set albumDuration=albumDuration + songDuration;
select albumDuration into totalDuration;
end loop getLength;
close songLength;

END$$

DELIMITER ;