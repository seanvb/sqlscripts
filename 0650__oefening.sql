USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DangerousInsertAlbumreleases`()
BEGIN
declare numberOfAlbums INT DEFAULT 0; 
declare numberOfBands INT DEFAULT 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
declare randomGen int;
declare teller int default 1;
declare exit handler for sqlexception
begin rollback;
select 'ERROR: Nieuwe releases konden niet worden toegevoegd.';
end;

set randomGen=rand()*3;
select randomGen;
start transaction;
while teller<=3 do

if (teller>1 and randomGen=1) then signal sqlstate '45000';
end if;
select count(*) into numberOfAlbums from albums;
select count(*) into numberOfBands from bands;
set randomAlbumId =floor(rand() * numberOfAlbums)+1;
set randomBandId=floor(rand()*numberOfBands)+1;

if ((select count(*) from Albumreleases where Bands_Id=randomBandId)<1 and (select count(*) from Albumreleases where Albums_Id=randomAlbumId)<1 ) then select concat('inserted', teller);
set teller= teller +1;
insert into albumreleases (Bands_Id,Albums_Id) values (randomBandId,randomAlbumId);
else signal sqlstate '45000';
end if;
end while;
commit;

END$$

DELIMITER ;